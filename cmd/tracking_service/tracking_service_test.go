/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"testing"
	"time"

	. "gitlab-templates-tracking/cmd/tracking_service/internal"
)

func TestBuildClient(t *testing.T) {
	configurations := "{\"clients\": [{\"url\": \"http://somewhere.com\",\"authentication\": {\"username\": \"username\",\"password\": \"p@ssw0rd!\"},\"timeout\": 5,\"indexPrefix\":\"tbc-\",\"esMajorVersion\":7,\"skipSslVerification\": true}]} "
	url := "http://somewhere.com"
	authentication := Authentication{
		Username: "username",
		Password: "p@ssw0rd!",
	}
	timeout := time.Duration(5)
	clients, _ := readConfiguration([]byte(configurations))

	client := clients.Clients[0]
	if client.Authentication.Username != authentication.Username || client.Authentication.Password != authentication.Password {
		t.Fatal("Authentication invalid")
	} else if client.Timeout != timeout {
		t.Fatalf("Invalid timeout. Expected %d got %d", timeout, client.Timeout)
	} else if client.Url != url {
		t.Fatal("Invalid url")
	}
}

func TestReadTemplateAndVersion(t *testing.T) {
	if err, arguments := readArguments("test", []string{"--skip_tracking", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.template != "template" && arguments.version != "version" {
		t.Fatalf("Unexpected template/version got: %s/%s", arguments.template, arguments.version)
	}
	if err, arguments := readArguments("test", []string{"--port", "8080", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.template != "template" && arguments.version != "version" {
		t.Fatalf("Unexpected template/version got: %s/%s", arguments.template, arguments.version)
	}
	if err, arguments := readArguments("test", []string{"--service", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.template != "template" && arguments.version != "version" {
		t.Fatalf("Unexpected template/version got: %s/%s", arguments.template, arguments.version)
	}
	if err, arguments := readArguments("test", []string{"--service", "--port", "8080", "--skip_tracking", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.template != "template" && arguments.version != "version" {
		t.Fatalf("Unexpected template/version got: %s/%s", arguments.template, arguments.version)
	}
}

func TestService(t *testing.T) {
	if err, arguments := readArguments("test", []string{"--service", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if !arguments.service {
		t.Fatal("Error while loading service flag")
	}
	if err, arguments := readArguments("test", []string{"template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.service {
		t.Fatal("Error while loading service flag")
	}
}
func TestSkipTracking(t *testing.T) {
	if err, arguments := readArguments("test", []string{"--skip_tracking", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if !arguments.skipTracking {
		t.Fatal("Error while loading skip tracking flag")
	}
	if err, arguments := readArguments("test", []string{"template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.skipTracking {
		t.Fatal("Error while loading skip tracking flag")
	}
}

func TestPort(t *testing.T) {
	if err, arguments := readArguments("test", []string{"--port", "8080", "template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.port != 8080 {
		t.Fatal("Error while reading port")
	}
	if err, arguments := readArguments("test", []string{"template", "version"}); err != nil {
		t.Fatal(err)
	} else if arguments.port != PORT {
		t.Fatal("Default port should be set if none precised")
	}
}
