/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"

	. "gitlab-templates-tracking/cmd/tracking_service/internal"
)

const PORT = 8039

type Configuration struct {
	Clients []ClientTracking `json:"clients"`
}

type runEnvironment struct {
	service      bool
	skipTracking bool
	port         int
	template     string
	version      string
}

func readConfigurationFromEnv() (Configuration, error) {
	config := os.Getenv("TRACKING_CONFIGURATION")
	if config != "" {
		return readConfiguration([]byte(config))
	}
	return Configuration{}, nil
}

func readConfiguration(config []byte) (Configuration, error) {
	var conf Configuration
	if config != nil {
		err := json.Unmarshal(config, &conf)
		return conf, err
	}
	return conf, nil
}

func readArguments(program string, args []string) (error, *runEnvironment) {
	var usage bytes.Buffer
	fs := flag.NewFlagSet(program, flag.ExitOnError)
	service := fs.Bool("service", false, "Launch send tracking as a service")
	skipTracking := fs.Bool("skip_tracking", false, "Does not send tracking")
	port := fs.Int("port", PORT, "Service port")
	fs.SetOutput(&usage)
	if err := fs.Parse(args); err != nil {
		fs.Usage()
		return errors.New(usage.String()), nil
	} else if fs.NArg() != 2 {
		usage.Write([]byte(fmt.Sprintf("%s: specify template name and version\n", program)))
		fs.Usage()
		return errors.New(usage.String()), nil
	}
	return nil, &runEnvironment{service: *service, skipTracking: *skipTracking, port: *port, template: fs.Arg(0), version: fs.Arg(1)}
}

func main() {
	var arguments []string
	if len(os.Args) == 1 {
		arguments = []string{}
	} else {
		arguments = os.Args[1:]
	}
	err, environment := readArguments(os.Args[0], arguments)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}
	config, err := readConfigurationFromEnv()
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err)
		os.Exit(2)
	}
	if !environment.skipTracking {
		log.Printf("Sending tracking for %s %s\n", environment.template, environment.version)
		for _, client := range config.Clients {
			if err := client.SendTracking(CiTrackingMessage(environment.template, environment.version)); err != nil {
				log.Println(err)
			} else {
				log.Println("Tracking sent")
			}
		}
	} else {
		if message, err := json.Marshal(CiTrackingMessage(environment.template, environment.version)); err != nil {
			log.Println(err)
		} else {
			log.Printf("%s\n", message)
		}
		log.Println("Skip send tracking")
	}
	if !environment.service {
		log.Println("Service not launched")
	} else {
		log.Printf("Launching service on port %d\n", environment.port)
		if err, server := HealthService(environment.port); err != nil {
			panic(err)
		} else {
			defer server.Close()
		}
	}
}
