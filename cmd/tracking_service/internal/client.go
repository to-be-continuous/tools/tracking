/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type Authentication struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ClientTracking struct {
	Url                 string          `json:"url"`
	IndexPrefix         string          `json:"indexPrefix"`
	ESMajorVersion      int             `json:"esMajorVersion"`
	Authentication      *Authentication `json:"authentication"`
	Timeout             time.Duration   `json:"timeout"`
	SkipSslVerification bool            `json:"skipSslVerification"`
}

func handleProxy() {
	log.Printf("'http_proxy' set to '%s'\n", os.Getenv("http_proxy"))
	log.Printf("'https_proxy' set to '%s'\n", os.Getenv("https_proxy"))
	log.Printf("'no_proxy' set to '%s'\n", os.Getenv("no_proxy"))
}

func init() {
	handleProxy()
}

func (client *ClientTracking) buildRequest(message *Message) (error, *http.Request) {
	var index, url string
	if client.IndexPrefix != "" {
		index = fmt.Sprintf("%s%d%02d", client.IndexPrefix, time.Now().Year(), time.Now().Month())
	} else {
		index = fmt.Sprintf("p19032_data_%d%02d", time.Now().Year(), time.Now().Month())
	}
	if client.ESMajorVersion >= 7 {
		url = fmt.Sprintf("%s/%s/_doc/", client.Url, index)
	} else {
		url = fmt.Sprintf("%s/%s/event", client.Url, index)
	}

	if payload, err := json.Marshal(*message); err != nil {
		return err, nil
	} else if request, err := http.NewRequest("POST", url, bytes.NewBuffer(payload)); err != nil {
		return err, nil
	} else {
		if client.Authentication != nil {
			request.SetBasicAuth(client.Authentication.Username, client.Authentication.Password)
		}
		request.Header.Set("Content-Type", "application/json")
		return err, request
	}
}

func (client *ClientTracking) sendRequest(request *http.Request) error {
	httpClient := &http.Client{}
	if client.Timeout != 0 {
		httpClient.Timeout = client.Timeout * time.Second
	}
	if response, err := httpClient.Do(request); err != nil {
		return err
	} else if (response.StatusCode / 100) != 2 {
		return fmt.Errorf("invalid status code %d", response.StatusCode)
	} else {
		return nil
	}
}

func (client *ClientTracking) SendTracking(message *Message) error {
	if client.SkipSslVerification {
		// nolint
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}
	if err, request := client.buildRequest(message); err != nil {
		log.Printf("Error while building request %+v\n", *message)
		return err
	} else if err := client.sendRequest(request); err != nil {
		log.Printf("Error while sending %+v\n", *message)
		return err
	} else {
		log.Printf("Message sent: %+v\n", *message)
		return nil
	}
}
