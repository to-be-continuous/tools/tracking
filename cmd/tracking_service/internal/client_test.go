/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"fmt"
	"testing"
	"time"
)

func TestBuildRequest(t *testing.T) {
	index := fmt.Sprintf("p19032_data_%d%02d", time.Now().Year(), time.Now().Month())
	client := ClientTracking{
		Url: "http://es-url",
		Authentication: &Authentication{
			Username: "username",
			Password: "P@ssW0rd!",
		},
		Timeout:             time.Duration(5),
		SkipSslVerification: true,
	}

	if err, request := client.buildRequest(&Message{
		MessageType:         "example",
		Timestamp:           "1981-01-17T19:45:00Z",
		TemplateDescription: TemplateDescription{"example", "6.6.6"},
		CiInformation: CiInformation{
			User: CiUser{
				Id:    111,
				Login: "buce8373",
			},
			Project: CiProject{
				Id:   222,
				Path: "/project",
			},
			Build: CiBuild{CommitReference: "commit-id"},
			Job: CiJob{
				Id:    333,
				Name:  "job-id",
				Stage: "build",
				Url:   "http://job-url/path",
			},
			Pipeline: CiPipeline{Id: 444, Url: "http://pipeline-url.com"},
			Runner: CiRunner{
				Id:          555,
				Description: "the runner",
				Tags:        "docker;shared",
				Version:     "7.7.7",
			},
		},
	}); err != nil {
		t.Fatal(err)
	} else if request.URL.Path != fmt.Sprintf("/%s/event", index) {
		t.Fatalf("Invalid path: %s", request.URL.Path)
	} else if request.Header.Get("Content-Type") != "application/json" {
		t.Fatalf("Invalid content type: %s", request.Header.Get("Content-Type"))
	} else if request.Header.Get("Authorization") == "" {
		t.Fatalf("Authorization not set")
	}
}
