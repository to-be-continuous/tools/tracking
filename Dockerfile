FROM registry.hub.docker.com/library/busybox:1.37.0 AS busybox

FROM scratch

ARG CI_PROJECT_URL
ARG TRACKING_CONFIGURATION=""
ENV TRACKING_CONFIGURATION="${TRACKING_CONFIGURATION}"

# hadolint ignore=DL3048
LABEL name="tracking"                                                    \
      description="Image for tracking 'to be continuous' templates usage"\
      url=$CI_PROJECT_URL                                                \
      maintainer="tbc-dev@googlegroups.com"


COPY bin/tracking_service       /tracking_service
COPY --from=busybox /bin/wget   /wget


EXPOSE  8039

HEALTHCHECK     CMD     ["/wget", "-Y", "off", "-O", "-", "http://localhost/health" ]

ENTRYPOINT [ "/tracking_service" ]

CMD [""]


